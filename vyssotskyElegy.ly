\version "2.25.15"

globalVyssotskyElegy = { 
  \clef "G_8"
  \key e\minor 
  \time 6/8
  \repeat volta 2 { s2.*8 } 
  \override Score.VoltaBracketSpanner.padding = 2
  \repeat volta 2 { s2.*6 } \alternative { { s2.*2 } { s2. } } 
  \mark\markup\fontsize #4 \segno
  \bar "||" s2. 
  \override Score.VoltaBracketSpanner.padding = 5
  \repeat volta 2 { s2.*4 } \alternative { { s2.*4 } { s2. } } s2.*3 \bar "||" 
  \tweak self-alignment-X #RIGHT
  \mark\markup\fontsize #-2 { "D.C. al" \lower #.3 \fontsize #4 \segno "et poi la Coda" } 
  \stopStaff \hideNotes \grace { b4 b } \unHideNotes \startStaff \bar ""
  \tweak extra-offset #'(-3 . .5)
  \tweak self-alignment-X #LEFT
  \mark\markup { \fontsize #2 \lower #.3 \coda \fontsize #-2 "CODA" } 
  \once\set Staff.forceClef = ##t
  \once\override Staff.Clef.full-size-change = ##t
  \once\override Staff.Clef.extra-offset = #'(.8 . 0)
  \once\override Staff.ClefModifier.extra-offset = #'(.8 . 0)
  \tweak extra-offset #'(1 . 0) \clef "G_8" 
  \key e\minor
  s2.*2 \bar "|."
}

voiceOneVyssotskyElegy = { 
  \voiceOne
  | % mes.1
    b'4.~ -\tweak extra-offset #'(-4 . 3.5) \mf 8 e'' b'
  | % mes.2
    ais'4. 4.
  | % mes.3
    a'!4.~ 8 b'\arpeggio fis'
  | % mes.4
    a'4. g'
  | % mes.5
    g'4.~ 8 <e' c''> g'
  | % mes.6
    fis'4. fis'
  | % mes.7
    fis'4.~ 8 <a dis' g'>\arpeggio dis'
  | % mes.8
    fis'4. e'
  | % mes.9
    d''4.~ 8 c'' b'
  | % mes.10
    b'4. a'
  | % mes.11
    c''4.~ 8 b' a'
  | % mes.12
    a'4. g'
  | % mes.13
    b'4.~ 8 a' g'
  | % mes.14
    g'4. fis'
  | % mes.15
    fis'4.~ 8 8\arpeggio e'
  | % mes.16
    e'4. dis'
  | % mes.17
    fis'4. a'8\rest b'\fermata dis'
  | % mes.18
    fis'4. e'
  | % mes.19
    fis'4.~ 8 g' a'
  | % mes.20
    a'4. g'
  | % mes.21
    gis'4.~ 8 a' b'
  | % mes.22
    b'4. a'
  | % mes.23
    e''4.~ 8 d'' c''
  | % mes.24
    c''4. b'
  | % mes.25
    g'4.~ 8 <e' fis'>\arpeggio\fermata e'
  | % mes.26
    e'4. dis'
  | % mes.27
    e''4.~ 8 fis'' e''
  | % mes.28
    b'4.~ 8 c'' b'
  | % mes.29
    g'4.~ 8 <dis' g'>\arpeggio dis'
  | % mes.30
    fis'4. e'
  | % stopStaff
    \grace s2 
  | % mes.31
    fis'4. e'8 g' b'
  | % mes.32
    <e e' e''>4.\arpeggio <g b e'>\arpeggio
}

voiceTwoVyssotskyElegy = { 
  \voiceTwo
  | % mes.1
    e,4. r
  | % mes.2
    cis4. fis
  | % mes.3
    dis4. fis
  | % mes.4
    e,2.
  | % mes.5
    c4. e,
  | % mes.6
    cis2.
  | % mes.7
    c!4. b,
  | % mes.8
    e,2.
  | % mes.9
    e,2.
  | % mes.10
    a,2.
  | % mes.11
    a4. d
  | % mes.12
    g4. a,\rest
  | % mes.13
    e,2.
  | % mes.14
    a,4. a,\rest
  | % mes.15
    cis4. c
  | % mes.16
    b,2.
  | % mes.17
    cis4. dis
  | % mes.18
    e,2.
  | % mes.19
    a,4. e,
  | % mes.20
    g4. a,\rest
  | % mes.21
    e,4. e
  | % mes.22
    a,4. a,\rest 
  | % mes.23
    a,4. a,\rest
  | % mes.24
    a,2.
  | % mes.25
    c4. cis
  | % mes.26
    b,2.
  | % mes.27
    a,4. a,\rest
  | % mes.28
    e,2.
  | % mes.29
    cis4. b,
  | % mes.30
    e,2.
  | % stopStaff
    \grace s2
  | % mes.31
    e,2.
  | % mes.32
    e,4.\arpeggio e,\arpeggio
}

voiceThreeVyssotskyElegy = { 
  \voiceFour
  | % mes.1
    g8\rest g b e' e'' b'
  | % mes.2
    b8\rest cis' e' d'\rest cis' fis'
  | % mes.3
    b8\rest c'! fis' d'\rest <a dis'>\arpeggio fis'
  | % mes.4
    g8\rest b e' g\rest b e'
  | % mes.5
    a8\rest c' e' g4.\rest
  | % mes.6
    a8\rest ais e' g\rest ais e'
  | % mes.7
    a8\rest a! dis' g4.\rest 
  | % mes.8
    g8\rest g b g\rest g b
  | % mes.9
    g8\rest b f' gis' c'' b'
  | % mes.10
    g8\rest c' e' g\rest c' e'
  | % mes.11
    f'8\rest ees' fis'! b4.\rest
  | % mes.12
    d'8\rest c' fis' b\rest b d'
  | % mes.13
    g8\rest g b e' a' g'
  | % mes.14
    g8\rest c' e' b\rest c' e'
  | % mes.15
    b8\rest ais e' b\rest <a e'>\arpeggio e'
  | % mes.16
    g8\rest fis a g\rest fis a
  | % mes.17
    b8\rest ais e' b\rest a dis'
  | % mes.18
    g8\rest g b g\rest g b
  | % mes.19
    g8\rest c' d'! g4.\rest
  | % mes.20
    d'8\rest c' fis'b\rest b d'
  | % mes.21
    g8\rest d' e' b4.\rest
  | % mes.22
    g8\rest c' e' b\rest c' e'
  | % mes.23
    g8\rest c' e' a' d'' c''
  | % mes.24
    g8\rest e' g' g\rest e' g'
  | % mes.25
    b8\rest ais e' b\rest a\arpeggio e'
  | % mes.26
    g8\rest fis a g\rest fis a
  | % mes.27
    g8\rest c' e' a' fis'' e''
  | % mes.28
    g8\rest g b e' c'' b'
  | % mes.29
    b8\rest ais e' g\rest a\arpeggio dis'
  | % mes.30
    g8\rest g b g\rest g b
  | % stopStaff
    \grace s2
  | % mes.31
    g8\rest g b e' g' b'
  | % mes.32
    s2. 
}

#(set-default-paper-size "letter")
#(set-global-staff-size 18)
\pointAndClickOff

\paper {
  system-count = 8
  ragged-last-bottom = ##f
  top-margin = 3
  bottom-margin = 6
  left-margin = 12
  right-margin = 12
  markup-system-spacing.basic-distance = 18
}

\header {
  title = \markup\fontsize #3 "Elegy"
  composer = "Attributed to Mikhail Vyssotsky"
  subtitle = " "
  poet = "Arranged for 6-string guitar by Matanya Ophee"
  tagline = \markup \override #'(baseline-skip . 2.3) \center-column \fontsize #-1 {
    \vspace #.5 \concat { "Copyright ©" \hspace #.1 "1997 Editions Orphée, Inc., Columbus, OH, 43235" }
    "International Copyright Secured. Made in U.S.A. All Rights Reserved."
    "Transcribed by Pierre Perol-Schneider using LilyPond 2.25.15"
  }
}

\score {
  \new Staff <<
    \globalVyssotskyElegy
    \new Voice \voiceOneVyssotskyElegy
    \new Voice \voiceTwoVyssotskyElegy
    \new Voice \voiceThreeVyssotskyElegy
  >>
  \layout {
    indent = 12
    \context {
      \Staff
      \consists Span_arpeggio_engraver
      connectArpeggios = ##t
      \override VoltaBracketSpanner.padding = #10
    }
  }
}